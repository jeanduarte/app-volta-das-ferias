let objDataPoint;
let chartQuantityPerKind;
let chartTotalQuantity;
let dataPoint = [];
let arrCategories = [];
let arrQuantity = [];
let arrQuantCategories = {};
let category;


localStorage.setItem("Saúde", 0);
localStorage.setItem("Alimentação", 0);
localStorage.setItem("Aluguel", 0);
localStorage.setItem("Lazer", 0);

window.onload = function () {

  
/* 
    let data = [
        {y:0, label:"Saúde"},
        {y:0, label:"Alimentação"},
        {y:0, label:"Aluguel"},
        {y:0, label:"Lazer"}
    ] */
    debugger
    getData();

    var dps=[];
    dps.push({label: "Saúde",       y: localStorage.getItem("Saúde") });
    dps.push({label: "Alimentação", y: localStorage.getItem("Alimentação") });
    dps.push({label: "Aluguel",     y: localStorage.getItem("Aluguel") });
    dps.push({label: "Lazer",       y: localStorage.getItem("Lazer") });
    /* var dps=[];
    let c = 0;
    data.forEach( (d)=>{        
        
        let X = d.y + arrQuantity[c];
        dps.push({label: d.label, y: localStorage.getItem(d.label) });
        c++;
    } );

 */
    chartQuantityPerKind = new CanvasJS.Chart("chartQuantityPerKind", {
        animationEnabled: true,
        theme: "light2",
        title:{
            text: "Quantidade por tipo"
        },
        axisY: {
            title: "Quantidade"
        },
        data: [{        
            type: "column",  
            showInLegend: true,             
            legendMarkerColor: "red",
            legendText: "Tipos(Categorias)",
            dataPoints: dps
        }]

    });    
    
    chartQuantityPerKind.render();
    
    //debugger
}


function getData(){
    let urlBase = "https://script.google.com/macros/s/AKfycbx5n3DVrYufZ1cxSJE6UKXvSn6lfhGagHgCiyFjzIkrhCto27Q/exec";

    window.fetch(urlBase)
        .then(function(response) {
            return response.json();
            
        
        }).then(function(body) {
            
            //quantidade de objetos
            let quantity =  Object.keys( body ).length;
            
            //captura todos as categorias para o array
            for(let c=0; c< quantity; c++){
                arrCategories[c] = body[c].categoria;                    
            }
            
            arrCategories.forEach( (category)=>{

                switch( category ){
                    
                    case 'Saúde':
                    localStorage.setItem("Saúde", parseInt(localStorage.getItem("Saúde")) + 1);
                    //arrQuantity[0] = arrQuantity[0] + 1;                            
                        break;

                    case 'Alimentação':
                    localStorage.setItem("Alimentação", parseInt(localStorage.getItem("Alimentação")) + 1);
                    //arrQuantity[1] = arrQuantity[1] + 1;                            
                        break;

                    case 'Aluguel':
                    localStorage.setItem("Aluguel", parseInt(localStorage.getItem("Aluguel")) + 1);
                    //arrQuantity[2] = arrQuantity[2] + 1;                            
                        break;

                    case 'Lazer':                        
                    localStorage.setItem("Lazer", parseInt(localStorage.getItem("Lazer")) + 1);
                    //arrQuantity[3] = arrQuantity[3] + 1;                            
                        break;
                }
            } );
            
        });
}
